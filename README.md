# TrafficControl

A Python 3 scalable UK LED-based traffic light hardware and software control
system using external logic and the Raspberry Pi.

## Summary

This project contains experimental Raspberry Pi software and hardware
schematics for the simulation of a scalable UK traffic light control system.
The objective is to produce a software and hardware solution that will be
capable of controlling a number of independent LED-based traffic lights with
associated path sensors in order to simulate traffic detection, control and
automated software switching of lights on a number of connected primary and
secondary routes.

    Each stable, tested release of the software is tagged.

## Requirements

To use this software you will also need one or more hardware traffic-light
modules detailed in the accompanying documentation.

This project was developed using the following hardware:

-	[Raspberry Pi] 2 Model B (Raspbian OS)
-	Additional bread-boards, wiring and logic devices
    as described for the prototype h/w
-   Python 3 (3.2 or better)
-   Python 3 RPi.GPIO module
-   Python 3 enum34 package

>   To run the non-hardware layers of the software (`TrafficControl` does run
    without modification *off target*) or tinker with or develop the
    upper-layers of the software you just need a Python 3.4 environment.
    The software was developed on OSX.
    
Shown below is a photo of the Rev-B hardware distributed amongst three
half-sized breadboards with the _decoder logic_ and one _register_ wired up to
two sets of LEDs (top right and bottom right). It is all mounted on a Laika kit
board but note that the Laika hardware is not used.

![Rev-B Hardware][Rev-B Hardware]

### Python 3.2

`Python 3.2.3` should be a standard part of the Raspberry Pi toolset. At the
very least, before installing `TrafficControl` you should update your
installation in the following (normal) way:

    sudo apt-get update
    
### Python 3 RPi.GPIO module

The Pi GPIO module is installed from the Raspian command-line:

    sudo apt-get install python3-rpi.gpio

### Python 3 enum34 package

`TrafficControl` uses *enumerations* present in Python 3.4. You can
install a *back-ported* version of the enumerations package from [PyPI].
You first need the `pip` package, after which you can download lots of
cool stuff.

    sudo apt-get install python3-pip
    
Installing PyPI may take a little while as a number of related developer
files are also installed. Once pip has been installed you can
add the Python 3.4 enumeration package with the following command:

    sudo pip-3.2 install enum34
 
## The TrafficController software modules

The solution, tested on the Raspberry PI is composed of the
following significant software modules:

-   `HardwareAccessLayer` is wrapper around the GPIO hardware. 
-   `Control` provides the lowest-level hardware independent definitions
    of the traffic hardware. It allows direct setting of each path's lights
    including implementation of standard sequences (i.e. stop-to-go).
-   `PathController` adds the higher-level and permits concurrent execution
    of the underlying 'Control' module. While the 'Control' adds the individual
    hardware instructions, 'PathController' adds the *business logic*,
    i.e. simplified control and concurrent access.
-   `PathAction` is a small module of commands used to control the
    thread execution of the 'PathController'. Use of the 'PathController'
    API results in individual commands, defined in 'PathAction' being passed
    to the internal business thread that passes calls to the 'Control' layer.

Everything except the `HardwareAccessLayer` is accompanied by a number of
unit and functional tests.

Additionally, wrapper-classes have been developed to provide simple control
of twin-path junctions (crossroads) and a Finite State Machine (FSM) framework
use by these classes to provide simple threaded control of concurrent
paths. These classes include:

-   `FiniteStateMachine` is an FSM framework.
-   Various `states` for taking path to green and red, a delay and
    other _waiting_ states.
-   `UnbiasedCrossroads`  for a crossroads that switches on demand
-   `OscillatingCrossroads` for a demo-style crossroads that constantly
    switches between the two paths.
    
And, finally, a `Demo` class that creates a single _oscillating_ crossroads
(described below).

## Example usage

Assuming you've constructed the accompanying digital hardware you can control
the two traffic paths independently using the software modules above.

Start Python 3 (with administrator privileges):

    $ sudo python3
    
From python you should then load the `Control` module in order to initialise
the Pi's GPIO ports:

    >>> import tcontrol.Control as ctrl
    >>> ctrl.init()
    
Once initialised you can create a `PathController` object to represent
each traffic path. You can create two if using the Rev-A hardware. Paths
are referred to using positive non-zero consecutive value. Rev-A supports
paths `1` and `2`.

    >>> import tcontrol.PathController as pc
    >>> path1 = pc.PathController(1)
    >>> path2 = pc.PathController(2)
    
The PathController objects implement the `threading.Thread` interface and
need to be started before they can be used. To start each path controller
as an independent thread you simply need to *start* them:

    >>> path1.start()
    >>> path2.start()
    
Now you can set the path lights to `stop()` or `go()`. This version of the
software operates traffic light transitions on a thread within the
`PathController` object. Consequently control is returned to the caller
immediately while the light transitions are handled in real-time
on a separate thread.

    >>> path1.go()
    
Then you can stop path 1 and release path 2:

    >>> path1.stop()
    >>> path2.go()
    
If you need to wait until a path change has actually been completed (i.e.
when the lights have reached their final state) you can call the
`wait()` method on the related `PathController` object. 

As the `PathController` objects are threads you can operated them independently
ans at the same time. An essential feature that will allow the concurrent
control of a number of independent traffic paths.

## Demo

For a simple 'oscillating' pair of traffic lights (using paths 1 and 2) try the
built-in Demo module which constantly cycles the lights:

    >>> import tcontrol.Demo as Demo
    >>> Demo.start()

And, when you're finished...

    >>> Demo.stop()

---

_Alan Christie - LIVR (July 2015)_

[PyPI]:             https://pypi.python.org/pypi
[Raspberry Pi]:		https://www.raspberrypi.org/products/raspberry-pi-2-model-b/
[Laika]:            http://www.project-laika.com/about-laika
[Rev-B Hardware]:   doc/hardware/rev-B-breadboard-hardware.jpg
