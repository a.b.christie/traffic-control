# TrafficControl (a project for the Raspberry Pi)

With a desire to add something and interactive to models
I present an architecture for modelling multi-junction traffic lights.
Why? Well, after my wife bought me a Raspberry Pi to while away the
wet weekends I was keen to exercise the programmable I/O of this device.
But doing what? Many of the kits that are available for the Pi provide you
with high-current drivers for things like motors and sensors but I didn't
(initially) want to build anything that they offered.

I've spent a long time in my career in software now and but I've always felt
my real skills lie in creating something that involved the use of or
design of digital hardware that _did something_ and _sensed something_.

Time passed.

I'm a keen collector of [Lego Creator], [Airfix] aircraft (especially
1:72 scale aircraft that served in the South-East Asia Command, SEAC) and
model railways. Having spent some time in the United States over various
festivals (4th July, Thanksgiving Halloween and Christmas) I was
immersed in their fun and almost child-like approach to these holidays and was
especially enamoured with their illuminated, interactive model Christmas
villages.

I have already started to collect a number of Lego Creator houses with the
intention of creating my own village scene. So far though this has simply
resulted in the accumulation of a number of kits that remain un-assembled
(well, until next Christmas anyway).

So, without any real competitive alternative I started to gravitate towards
constructing some interactive lighting for a Lego Christmas village, especially
lights with sensors to detect the presence of objects. After all, what can be
more interactive than a fully working set of traffic lights? They'll
provide some pretty and dynamic illumination to a Christmas scene and,
ideally would use some form of sensors to detect traffic and change
lights automatically with timing based on demand.

The outline goal of the hobby project was becoming clear and the following
self-imposed requirements were emerging:-

1.  A modular hardware solution that would support a number of junction types
    (crossroads, 3-way junctions, temporary traffic lights).
1.  Accommodate input sensors to detect traffic
1.  Minimise the demands on GPIO pins
1.  The project should be GitHud-based and open source
1.  Software developed using Python 3
1.  Use a structured test-driven software design employing Travis CI and
    Coveralls code coverage
1.  Compartmentalise the hardware driver layer so that the vast majority of the
    software can be developed and tested off-target (i.e. on a Mac or PC)
1.  Contain a user-guide and hardware diagrams
1.  Prototype hardware would be bread-boarded with the intention of
    producing a strip-board solution and realistic lights for the
    Christmas model.
     
You can find this project's source code, documentation and hardware
design on its [GitLab] page.

## What will I need?

I have tried to document the complete hardware and software design here so
that it can form the basis of your own design. To recreate this project you
will need the following:-

-   A Raspberry Pi (ideally a Model B+)
-   Breadboards (3 half sized)
-   Breadboard wires
-   LEDs, resistors and 74-series integrated circuits
-   A multimeter for pre-power testing
-   A logic probe for post-power testing

## The design

Let's look at our options and explore the design.

### Before you continue

As this project involves attaching external hardware to your Pi
you should take sufficient steps to understand the implications and risks
of designing and attaching your own hardware to the Pi or indeed any
computer. Incorrect wiring and short-circuits are likely to permanently
damage your Pi.

One note of caution: take extreme care when working with GPIO on the Pi.
The header pins are invariably attached directly to the Broadcom chip,
short circuits and wiring faults may damage your Pi. The GPIO pins are also
3.3v and are unlikely to be protected against over-voltage. You should
take care when driving the GPIO pins (as inputs) from 5v logic devices.

    Always take extremely care and check your wiring and then check it again.
    Never adjust the wiring when power is applied. One mistake may damage
    your Pi.

Having read all this, you might be reluctant to do anything with hardware but,
as long as you understand what you're doing, have some understanding of
digital logic and basic design skills and take generally sensible steps
to development (i.e. checking your wiring) it's the hardware that makes
software exciting - after all, that's why you bought your Pi, isn't it?

    Thanks to [Matt Hawkins] for his blog pages on the Pi and its hardware.

## GPIO

GPIO (General Purpose Input/Output) provides pins that can be
software-configured as inputs that can be used to read an external digital\
_bit_ or as outputs translating a single digital _bit_ into a voltage,
0v or _ground_ for `zero` and approximately +3.3v for `one`.

GPIO is our path to interfacing the Pi to the _real world_ of LEDs and sensors.
In this project we'll be connecting GPIO output pins (indirectly) to LEDs
and using some pins as inputs to read sensors.

The A+, B+ and Pi 2 expose their GPIO pins using a convenient 40-pin header.
The 40 header pins provide the following services:-

-   8 earth or ground pins (0v)
-   2 5v power pins (supplying 300mA from a 1A power supply)
-   2 3.3v power pins (supplying 50mA)

Some of the remaining 28 pins provide the following functions:

-   I2C (inter-Integrated Circuit) bus
-   SPI (Serial Peripheral Interface)
-   UART (Universal Asynchronous Receiver/Transmitter)

Of the remaining pins 11 should be available for interfacing our hardware
without interfering any built-in features. The Broadcom pins available to us
are as follows (with the physical header pin number in brackets):

-   BCM 5 (29)
-   BCM 6 (31)
-   BCM 12 (32)
-   BCM 13 (33)
-   BCM 16 (36)
-   BCM 17 (11)
-   BCM 22 (15)
-   BCM 23 (16)
-   BCM 24 (18)
-   BCM 25 (22)
-   BCM 26 (37)

We'll be using 6 pins, configured as outputs to address 16 separate sets of
traffic lights that have associated input sensors or 32 separate sets of
lights without that have no input sensors. We'll be using ports 5, 6, 12
and 22, 23, 24, 25, 26.

    The Raspberry Pi [Pinout] site provides comprehensive documentation
    on the designation of the Pi's GPIO pins.

## Yes, you can drive LEDs from the GPIO bus, but...

How much hardware do we need? After all, we have a Raspberry Pi and its
General Purpose I/O (GPIO) bus.

That's right, you can indeed drive LEDs directly from the GPIO pins. They're
3.3V and can source sufficient current for an LED (with a suitable current
limiting resistor as well discuss later). But, forced to use a GPIO pin
for each LED, with only 11 or so pins free we barely have
enough pins to make 3 sets of independent lights.

What we're designing here is a scalable system that accommodates a larger
number of independent lights, between 16 and 32.

Connecting the LEDs directly to the GPIO bus is not an option for this design.

## A basic (single) set of lights

It's obvious that a single set of traffic lights has three lamps
that need to be lit independently - i.e the RED light illuminates without
the AMBER and the AMBER illuminates independently of the GREEN.
Our traffic light wil consist of three LEDs and three wires to supply the
current to each, with the current returning via the common (earth) return wire.

### The basic LED circuit

### 74-series open-collector devices

We will be using 74-series, 5V TTL gates for the external logic of this design.
These devices are powered by a 5V supply and there are a large number logic
devices to build even the most complex digital system. In fact, with a
sufficiently large number of 74-series devices you could produce your
own central processing unit.

When the logic inpu tis low on the 74LS07 the output wil be low,
and current wil flow from the supply voltage, through the current-limiting
resistor and LEDs and into the connected 74LS07 gate. The current the device
can _sink_ in this way can be up to 40mA

The 74LS07 can therefore easily _sink_ current from more than one LED.

### Connecting multiple LEDs

### Capacity estimates

For a given pair of lights, opposite lights on a crossroads, the most current
drawn wil be on the RED-AMBER sequence as RED progresses to GREEN. Here
4 LEDs wil be illuminated, drawing approximately 80mA.

If we do create 8 sets of crossroads with two paths each we'll have
16 pairs of traffic lights. If we assume each crossroads simultaneously
progresses to RED-AMBER these light wil draw approximately 8 x 80mA, or
640mA. If we include the 8 opposing lights (which wil be on RED) they will
contribute and additional 8 x 20mA, or 160mA. In total, our 8 junctions
can draw around 640mA + 160mA, or 800mA (0.8A).

You can see that the lamps of 8 junctions wil contribute ro nearly 1 amp,
excluding the power drawn by the surrounding digital logic.

These calculations are important to make sure that we provide a power-supply
capable to providing sufficient current to all parts of our circuit.

Our LEDs are likely to be the largest consumer of power, the digital logic,
as wee wil see will make relatively small demands on power.

### The encoder

### The memory register (flip-flop)

### Power-on reset

## A simple 4-way junction

## Temporary lights (for roadworks)

## Building the hardware

### 3.3v and 5v considerations

### The register

### The decoder

### The hardware access layer (HAL) module

### The control module

### Testing

## Higher-level software functions

### The PathController

### States and state machines

## The state machines

### Oscillating crossroads

### Unbiased crossroads

## The 'I just want a demonstration' demo

The project is simply encapsulated in the `Demo.py` module. This small piece
of software is designed to quickly demonstrate a pair of traffic lights
by creating an `OscillatingCrossroads` (see the main README file for details).

---

_Alan Christie - LIVR (July 2015)_

[GitLab]:       https://gitlab.com/a.b.christie/traffic-control
[Lego creator]: http://www.lego.com/en-gb/creator
[Airfix]:       http://www.airfix.com/uk-en/aircraft/1-72-scale-military-aircraft.html
[Matt Hawkins]: http://www.raspberrypi-spy.co.uk/2012/06/simple-guide-to-the-rpi-gpio-header-and-pins/
[Pinout]:       http://pi.gadgetoid.com/pinout
