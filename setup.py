#!/usr/bin/env python

# Setup module for the Traffic Control software
#
# Jun 2015

from setuptools import setup

# Read the requirements.txt file (used by Travis CI)
# ans use for setup's install_requires[] list.
with open('requirements.txt') as f:
    required = f.read().splitlines()

# ------------------------------------------------------------------------------
# setup
# ------------------------------------------------------------------------------
setup(

    name='TrafficControl',
    version='2.0.0',
    platforms=['any'],

    url='https://gitlab.com/a.b.christie/traffic-control',
    license='http://www.apache.org/licenses/LICENSE-2.0',
    author='Alan Christie',
    description='Traffic control software',
    long_description='A scalable UK traffic light hardware and software'
                     ' control system for the associated TrafficControl'
                     ' hardware. This package will interface to a configurable'
                     ' number of independent traffic lights and path sensors'
                     ' to simulate the control of light and heavy traffic'
                     ' assuming a number of traffic-light-controlled primary'
                     ' and secondary routes.',

    test_suite='tcontrol.test',

    # Installation dependencies.
    # This ia the list of registered PyPI modules that we use.
    install_requires=required,

    # Our packages here.
    # Specific modules can be found in the MANIFEST.in file.
    packages=['tcontrol', 'tcontrol.test'],

    # Project classification...
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3.4',
        'Topic :: Games/Entertainment :: Simulation'
    ]

)

