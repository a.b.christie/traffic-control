# TopicServer Release Notes

## TDB

-  Added breadboard photo and reference to it in the README  

## 2.0.0

-  Support for Rev-B hardware.
   1.0.1 is the last version to support Rev-A hardware

## 1.0.1

-   Renamed `HardwareControlLayer`.
    There were too many files with the word 'Control'.
    The Module is now called `HardwareAccessLayer`
-   Minor changes to `Control` (use of time rather than threading)
-   Typo and doc fixes
-   Added FSM, states and UnbiasedCrossroads wrapper for 2-path junction
-   Added OscillatingCrossroads and StateDelay
-   Adds simple debug/trace to FSM
-   Adds Demo.py
    
## 1.0.0

-   First working thread-based `PathController` architecture.
    Tested on Raspberry Pi 2 (Raspbian) and a bread-boarded
    version of the `traffic-light-register` (Rev-A) hardware.
    
---

_Alan Christie - LIVR (July 2015)_
