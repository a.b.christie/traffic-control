#!/usr/bin/env python3

"""
UK traffic control simulation software.

This software is designed to work with the _Traffic Light Decoder_ and
_Traffic Light Register_ 'Rev B' hardware (see the hardware documentation in
`doc/hardware` for details of the project's hardware).

Alan Christie
July 2015
"""

# Include standard Python stuff...
import time

# Import our stuff...
import tcontrol.HardwareAccessLayer as HardwareAccessLayer

# Module version
# and brief revision history (latest first)
__version__ = '1.1'
#   1.1     Use of time.sleep() rather than timers
#   1.0     Initial (prototype) release

# Period between individual light state transitions.
# This is the (approximate) period between the transition
# from one lit state (red) to the next (red-amber).
# The total time to switch a path from 'go' to 'stop'
# is (approximately) twice this figure.
_TRANSIT_TIME_S = 2.5


# -----------------------------------------------------------------------------
def init():

    """Initialises the hardware layer.
    The underlying hardware access layer should protect
    itself from being called more than once."""

    HardwareAccessLayer.init()


# -----------------------------------------------------------------------------
def red(path):

    """Set the lights for the named path to red."""

    HardwareAccessLayer.set_path(path, 0, 0)


# -----------------------------------------------------------------------------
def green(path):

    """Set the lights for the named path green."""

    HardwareAccessLayer.set_path(path, 1, 1)


# -----------------------------------------------------------------------------
def amber(path):

    """Set the lights for the named path to amber."""

    HardwareAccessLayer.set_path(path, 1, 0)


# -----------------------------------------------------------------------------
def red_amber(path):

    """Set the lights for the named path to red-amber."""

    HardwareAccessLayer.set_path(path, 0, 1)


# -----------------------------------------------------------------------------
def go(path):

    """Assuming the lights for the named path are red, calling this
    progresses them in a natural timed order to green.

    This method blocks until the transition to green is complete."""

    time.sleep(_TRANSIT_TIME_S)
    red_amber(path)
    time.sleep(_TRANSIT_TIME_S)
    green(path)


# -----------------------------------------------------------------------------
def stop(path):

    """Assuming the lights for the named path are green, calling this
    progresses them in a natural timed order to red.

    This method blocks until the transition to red is complete."""

    time.sleep(_TRANSIT_TIME_S)
    amber(path)
    time.sleep(_TRANSIT_TIME_S)
    red(path)


# -----------------------------------------------------------------------------
def reset():

    """Resets the state of all paths to red."""

    for path in range(1, num_paths() + 1):
        red(path)


# -----------------------------------------------------------------------------
def num_paths():

    """Returns the number of addressable paths."""

    return HardwareAccessLayer.num_paths()


# -----------------------------------------------------------------------------
def get_last_set_data():

    """Returns the last 'set_path()' values processed by the
    underlying access control layer. This method is used by
    unit-tests."""

    return HardwareAccessLayer.get_last_set_path_data()
