#!/usr/bin/env python3

"""
The hardware access layer (HAL) for the UK traffic control simulation software.

This software is designed to work with the _Traffic Light Decoder_ and
_Traffic Light Register_ 'Rev B' hardware (see the hardware documentation in
`doc/hardware` for details of the project's hardware).

Alan Christie
July 2015
"""

# Include standard Python stuff...
import threading

# Module version
# and brief revision history (latest first)
from tcontrol import PathBank

__version__ = '1.1'
#   1.1     Support for Rev-B hardware
#   1.0     Initial (prototype) release

# (Try to) Import Raspberry Pi hardware driver stuff...
# If we cannot find it then clear `have_gpio`
try:
    import RPi.GPIO as GPIO
    have_gpio = True
except ImportError:
    have_gpio = False

# The largest path number of the decoder logic
_TLD_MAX_PATH = 16

# GPIO pin designations for the hardware
#
# Traffic Light Decoder (TLD) pins...
# Address pins
_TLD_A0 = 24
_TLD_A1 = 25
_TLD_A2 = 26
# Selector pins
_TLD_SEL0 = 5
_TLD_SEL1 = 6
_TLD_SEL2 = 12
#
# Traffic Light Register (TLR) pins...
# Data line A
_TLR_A = 22
# Data line B
_TLR_B = 23

# Set when the module is initialised
_initialised = False

# A lock object, to protect multi-threaded access to GPIO
_lock = None

# Data, retained for unit-test purposes.
# Initialised to `-1`, an invalid value
_last_a = -1
_last_b = -1
_last_path = -1

# Set if a path has been selected.
# This is cleared when the path has been cleared.
# This is set by `_set_decoder()`
# and is cleared by `_clr_decoder()`
_path_selected = False


# -----------------------------------------------------------------------------
def init():

    """Initialise the GPIO hardware by setting port designation for input
    and output.

    If the GPIO module couldn't be loaded this safely continues (so the code
    builds and runs off-target) but does nothing to any hardware.

    I initialisation has been successful all traffic light control paths
    are set to RED on each call to this method."""

    global _initialised
    global _lock

    # Do nothing if already initialised.
    # We cannot re-initialise.
    if not _initialised:

        # Create a synchronisation lock.
        # This lock is used to protect
        # access to the hardware.
        _lock = threading.Lock()

        # Only tinker with GPIO if we have it...
        if have_gpio:

            # Use BCM mode - so port numbers
            # are those used on the header.
            GPIO.setmode(GPIO.BCM)
            # Ignore warnings.
            # This prevents further setup() calls
            # from generating warnings.
            GPIO.setwarnings(False)

            # Set the A & B lines as output
            GPIO.setup(_TLR_A, GPIO.OUT)
            GPIO.setup(_TLR_B, GPIO.OUT)
            # Set address lines as output
            GPIO.setup(_TLD_A0, GPIO.OUT)
            GPIO.setup(_TLD_A1, GPIO.OUT)
            GPIO.setup(_TLD_A2, GPIO.OUT)
            # Set selector lines as output
            GPIO.setup(_TLD_SEL0, GPIO.OUT)
            GPIO.setup(_TLD_SEL1, GPIO.OUT)
            GPIO.setup(_TLD_SEL2, GPIO.OUT)

        # Now mark as initialised...
        _initialised = True

    # Now set all the path lights to RED
    for path in range(1, 17):
        set_path(path, False, False)
    

# -----------------------------------------------------------------------------
def set_path(path, a, b):

    """Sets the lights for the named path to the value defined by `a` and `b`.

    `a b state`
    `-------------`
    `F F red`
    `F T red-amber`
    `T F amber`
    `T T green`

    If the GPIO module couldn't be loaded this safely continues
    (so the code builds and runs off-target) but does nothing
    to any hardware."""

    global _last_a
    global _last_b
    global _last_path
    assert _initialised

    # Hardware *must be* initialised.
    assert _initialised

    # Valid strobe ID? Must be 1..N
    assert 0 < path <= _TLD_MAX_PATH,\
        "Invalid path ID (%s)" % path

    # Remember A & B, the path and the path port
    # (we do this for unit testing)
    _last_a = a
    _last_b = b
    _last_path = path

    # Only tinker with hardware if we have the GPIO library...
    if have_gpio:

        assert _lock, "No lock instance"
        with _lock:

            # Set the data lines (A & B)
            GPIO.output(_TLR_A, a)
            GPIO.output(_TLR_B, b)
            # Strobe the chips on the decoder
            # so the data lines are clocked into the register.
            _strobe_data(path)


# -----------------------------------------------------------------------------
def _strobe_data(path):

    """Clocks the data on the A and B ports (defining the traffic light
    condition) into the appropriate traffic-light register."""

    # Valid path ID? Must be 1..N
    assert 0 < path <= _TLD_MAX_PATH,\
        "Invalid decoder path ID (%s)" % path

    # To clock the data into the registers briefly
    # set, then clear the decoder for the path...
    _set_decoder(path, PathBank.PathBank.a)
    _clr_decoder()


# -----------------------------------------------------------------------------
def _set_decoder(path, bank):

    """Sets the named path for the traffic-light decoder bank (a or b)."""

    global _path_selected

    # Valid path ID? Must be 1..N
    assert 0 < path <= _TLD_MAX_PATH,\
        "Invalid decoder path ID (%s)" % path

    # Decoder must be de-selected.
    # If not a path has been set but not cleared.
    assert not _path_selected,\
        "Decoder is selected"

    # Mark entry to a `set` method.
    # This is cleared by a call to `clr_decoder()`
    _path_selected = True

    # To set a path, avoiding any transient selections,
    # first set A0-A2 then set SEL0-SEL2

    # Set A0 - A1
    _set_tld_address(path)

    # Now set the (chip) selector pins (SEL0 - SEL2).
    # This has to be done safely, in an order that prevents
    # any other path (on A or B) from being selected.

    # We must select the bank first.
    # If we're setting the path for bank A (IC1 or IC2) SEL2 is high (1)
    # and nothing needs to be done to SEL2 (it's already high).
    # If we're setting the path for bank B (IC3 or IC4) SEL2 is low (0).
    # If we set SEL0 or SEL1 first we'll inadvertently
    # (briefly) enable a path on path A (IC1 or IC2).
    if bank == PathBank.PathBank.b:
        GPIO.output(_TLD_SEL2, 0)

    # Now that we've enabled to appropriate bank
    # we can elect the 1-of-8 decoder chip. This is IC1 or IC3 for paths 1-8
    # or IC2 or IC4 for paths 9-16.
    if path <= 8:
        # SEL0 = 0
        # SEL1 = 1
        # By default all selector pins are high (1)
        # here, we simply need to write a low to selector 0
        GPIO.output(_TLD_SEL0, 0)
    else:
        # SEL0 = 1
        # SEL1 = 0
        # By default all selector pins are high (1)
        # here, we simply need to write a low to selector 1
        GPIO.output(_TLD_SEL1, 0)


# -----------------------------------------------------------------------------
def _clr_decoder():

    """Clears any path selection made on decode A.
    After this call all output path (PA1-PA16)
    will be in an *unselected* (high) state."""

    global _path_selected

    # To disable the decoders we set all selectors to high (1)
    # To do this safely we must first raise SEL0  and SEL1
    # before raising SEL2.
    GPIO.output(_TLD_SEL0, 1)
    GPIO.output(_TLD_SEL1, 1)
    GPIO.output(_TLD_SEL2, 1)

    # Mark exit from the `clr` method.
    # This is set by a call to the `set` method.
    _path_selected = False


# -----------------------------------------------------------------------------
def _set_tld_address(path):

    """Sets the address lines (A0-A1) on the decoder.
    This is the first step in enabling PA1-PA16 or PB1-PB16"""

    # Valid path ID? Must be 1..N
    assert 0 < path <= _TLD_MAX_PATH,\
        "Invalid decoder path ID (%s)" % path

    # The decoder path is zero-based
    # i.e. path - 1
    decoder_path = path - 1
    GPIO.output(_TLD_A0, decoder_path & 1 > 0)
    GPIO.output(_TLD_A1, decoder_path & 2 > 0)
    GPIO.output(_TLD_A2, decoder_path & 4 > 0)


# -----------------------------------------------------------------------------
def num_paths():

    """Returns the number of addressable paths
    (in any particular direction)."""

    return _TLD_MAX_PATH


# -----------------------------------------------------------------------------
def get_last_set_path_data():

    """For unit testing we return the last A and B line values
    and the path number."""

    return _last_a, _last_b, _last_path
