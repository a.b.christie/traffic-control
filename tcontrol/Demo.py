#!/usr/bin/env python3

"""
A Demo which creates a single OscillatingCrossroads instance.
Start the demo with a call to start() and stop it by calling stop.

Alan Christie
July 2015
"""

# Import traffic-control stuff...
import tcontrol.Control as Control
import tcontrol.OscillatingCrossroads as OscillatingCrossroads
import tcontrol.PathController as PathController

# Demo objects...
pri = PathController.PathController(1)
sec = PathController.PathController(2)
demoOc = OscillatingCrossroads.OscillatingCrossroads(pri, sec)
# Uncomment to enable underlying FSM debug...
#demoOc.debug()

Control.init()


# -----------------------------------------------------------------------------
def start():

    """Starts (runs) the demo."""

    demoOc.start()


# -----------------------------------------------------------------------------
def stop():

    """Stops the demo."""

    demoOc.stop()
