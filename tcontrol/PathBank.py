#!/usr/bin/env python3

"""
Traffic management 'PathBank' enumerations.

Alan Christie
July 2015
"""

from enum import Enum


# -----------------------------------------------------------------------------
# PathBank
# -----------------------------------------------------------------------------
class PathBank(Enum):

    """An enumeration used in the HardwareAccessLayer
    to identify the path bank (a or b)."""

    a = 1
    b = 2
