#!/usr/bin/env python3

"""
TrafficControl state implementation.
This class (for use with FiniteStateMachine) introduces a delay.

Alan Christie
July 2015
"""

# Import Python stuff
import datetime


# -----------------------------------------------------------------------------
# StateDelay
# -----------------------------------------------------------------------------
class StateDelay:

    """State representation for a delay. The state simply waits until
    sufficient time has elapsed before yielding the next state."""

    # -------------------------------------------------------------------------
    def __init__(self, delay_seconds, next_state):

        """Initialises the state object."""

        # And must have a next state
        assert next_state,\
            "No next state"

        self.delay = datetime.timedelta(seconds=delay_seconds)
        self.next_state = next_state
        self.enter_time = datetime.datetime.now()

    # -------------------------------------------------------------------------
    def enter(self):

        """State entry implementation."""

        # Record the state entry time.
        # When 'delay' seconds elapses the state yields
        # the next state during the next call to notify()

        self.enter_time = datetime.datetime.now()

    # -------------------------------------------------------------------------
    @staticmethod
    def exit():

        """State exit implementation."""

        # There's nothing to do
        # when we leave this state
        pass

    # -------------------------------------------------------------------------
    def notify(self):

        """State notify implementation. This is called at a regular rate.
        Here we yield the next state when sufficient time has elapsed."""

        if datetime.datetime.now() - self.enter_time >= self.delay:
            return self.next_state
