#!/usr/bin/env python3

"""
A simple finite state machine (FSM).

Alan Christie
July 2015
"""

from threading import Timer

# Module version
# and brief revision history (latest first)
__version__ = '1.0'
#   1.0     Initial (prototype) release

# Approximate period between consecutive calls to 'notify()'
_NOTIFY_PERIOD = 1.0


# -----------------------------------------------------------------------------
# FiniteStateMachine
# -----------------------------------------------------------------------------
class FiniteStateMachine:

    """A finite sate machine (FSM)."""

    # -------------------------------------------------------------------------
    def __init__(self):

        """Initialise the FSM. debug_context is used when debugging
        to distinguish trace between FSMs. Using a different value for each
        FSM will allow you do filter trace based on FSM."""

        self.states = {}
        self.current_state = None
        self.current_state_name = None
        self.started = False
        self.notify_timer = None
        self.notify_count = 0

        # A value used to distinguish between debug from multiple FSMs
        self.debug_context = 0

        # Flag, set by calling debug() to debug FSM transitions
        self._debug = False

    # -------------------------------------------------------------------------
    def add_state(self, state_implementor, state_name):

        """Adds a new state to the FSM.
        THe FSM must not be running - all state implementors
        need to have been added before the FSM is started. Once started
        The FSM cannot be modified."""

        assert not self.started, \
            "Cannot add a state after the FSM has been started."

        # The supplied implementor
        # must have 'notify', 'enter' and 'exit' methods
        try:
            callable(state_implementor.notify)
        except AttributeError:
            assert False, \
                "State has no notify() method"
        try:
            callable(state_implementor.enter)
        except AttributeError:
            assert False, \
                "State has no enter() method"
        try:
            callable(state_implementor.exit)
        except AttributeError:
            assert False, \
                "State has no exit() method"

        # Record the state (unless it's already known)
        assert state_name not in self.states.keys(),\
            "State name already used"

        # The first state is also the initial state
        if not self.current_state:
            self.current_state = state_implementor
            self.current_state_name = state_name
        # Extend the list of states
        self.states[state_name] = state_implementor

    # -------------------------------------------------------------------------
    def start(self):

        """Starts the FSM. This is called when all the states have been
        added. Once started the FSM cannot be modified.

        When started a timer is created which calls this object's
        `_notify()` which calls the current state's `notify()` method.
        The timer is re-scheduled on each notify call until the FSM is
        stopped."""

        if not self.started:

            assert self.current_state,\
                "Cannot start without states."

            self._trace("Starting...")

            # Fire the first state's 'enter()' method...
            self.current_state.enter()
            # Setup a timer...
            self.notify_timer = Timer(_NOTIFY_PERIOD, self._notify)
            self.notify_timer.start()
            self.started = True

    # -------------------------------------------------------------------------
    def stop(self):

        """"Called to stop the FSM. If not started, no action is taken."""

        if self.started:

            self._trace("Stopping...")

            if self.notify_timer:
                self.notify_timer.cancel()
            self.started = False

    # -------------------------------------------------------------------------
    def _notify(self):

        """The FSM `_notify()` method. This method is called at regular
        intervals when the FSM is started. The `_notify()` method calls
        the current state's `notify()` method which optionally returns the
        name of a new state. If a new state is requested by the current state
        the current state's `exit()` method is called before the state is
        replaced before the new state's `enter()` method.

        This `_notify()` method is called at regular intervals until the FSM
        is stopped."""

        assert self.current_state, \
            "Missing current state"

        # Invoke the state's notify method.
        # If a state-change takes place the notify() method
        # returns the name of the new state, otherwise it returns None.
        next_state = self.current_state.notify()

        # Move state?
        # If so we call the current state's exit() method
        # set the new state and call it's entry method.
        if next_state:

            assert next_state in self.states,\
                "Next state is not known"

            self._trace("Moving to next state '%s'" % next_state)

            self.current_state.exit()
            self.current_state = self.states[next_state]
            self.current_state_name = next_state
            self.current_state.enter()

        # Count
        self.notify_count += 1
        # Reschedule ourselves..?
        if self.started:
            self.notify_timer = Timer(_NOTIFY_PERIOD, self._notify)
            self.notify_timer.start()

    # -------------------------------------------------------------------------
    def get_num_states(self):

        """Return the number of known states.
        A method added to aid unit testing"""

        return len(self.states)

    # -------------------------------------------------------------------------
    def get_current_state_name(self):

        """Return the name of the current state
        (None if no state is current).
        A method added to aid unit testing"""

        return self.current_state_name

    # -------------------------------------------------------------------------
    def _trace(self, msg):

        if self._debug:
            print('FSM-%d> %s' % (self.debug_context, msg))

    # -------------------------------------------------------------------------
    def debug(self, context=0):

        """Sets the debug flag, which enables trace of FSM transitions."""

        self._debug = True
        self.debug_context = context
