#!/usr/bin/env python3

"""
TrafficControl state implementation.
This class (for use with FiniteStateMachine) manages a path's transition
to Red (Stop). When done the notify method yields the name of the next state.

Alan Christie
July 2015
"""

# Import our stuff
import tcontrol.PathController as PathController


# -----------------------------------------------------------------------------
# StatePathGoToRed
# -----------------------------------------------------------------------------
class StatePathGoToRed:

    """State representation for lights transitioning to red. The `enter()`
    method calls the path `stop()` method and the `notify()` method waits
    until the transit is complete releasing control to the `next_state`.
    The `exit()` method does nothing."""

    # -------------------------------------------------------------------------
    def __init__(self, path, next_state):

        """Initialise the state object."""

        # We must be given PathControllers
        assert isinstance(path, PathController.PathController),\
            "Path is not a PathController"
        # And must have a next state
        assert next_state,\
            "No next state"

        self.path = path
        self.next_state = next_state

    # -------------------------------------------------------------------------
    def enter(self):
        
        """State entry implementation. Here, we start the transition
        to 'green'."""
        
        assert self.path

        self.path.stop()

    # -------------------------------------------------------------------------
    @staticmethod
    def exit():

        """State exit implementation."""
        
        # There's nothing to do
        # when we leave this state
        pass

    # -------------------------------------------------------------------------
    def notify(self):

        """State notify implementation. This is called at a regular rate.
        Here we wait until the transition to 'red' has taken place.
        When it has, we transit to the next state by returning the
        next state name to the caller."""
        
        if not self.path.wait(0):

            # Transition complete.
            # Move to next state.
            return self.next_state
