#!/usr/bin/env python3

"""
TrafficControl state implementation.

Alan Christie
July 2015
"""

# Import standard python stuff
import datetime

# Import our stuff
import tcontrol.PathController as PathController


# -----------------------------------------------------------------------------
# StatePathWaitForDemand
# -----------------------------------------------------------------------------
class StatePathWaitForDemand:

    """State representation for light transition tests.  The `enter()`
    method does nothing. The `notify()` method tests for `demand` on the
    state's path. When demand is detected the notify method yields the next
    state. The `exit()` method resets internal member variables."""

    # -------------------------------------------------------------------------
    def __init__(self, path, hold_time_seconds, next_state):

        """Initialise the state."""

        # We must be given PathControllers
        assert isinstance(path, PathController.PathController),\
            "Path is not a PathController"
        # And must have a next state
        assert next_state,\
            "No next state"

        self.path = path
        self.next_state = next_state
        self.hold_time = datetime.timedelta(seconds=hold_time_seconds)
        self.demand = None

    # -------------------------------------------------------------------------
    @staticmethod
    def enter():

        """State entry implementation."""

        # There's nothing to do
        # when we leave this state
        pass

    # -------------------------------------------------------------------------
    def exit(self):

        """State exit implementation."""

        # TReset demand time
        self.demand = None

    # -------------------------------------------------------------------------
    def notify(self):

        """State notify implementation. This is called at a regular rate.
        Here we wait until the transition to green has taken place.
        When it has, we transit to the next state by returning the
        next state name to the caller."""

        if self.demand:

            # We've had demand.
            # Have we waited long enough?
            if datetime.datetime.now() - self.demand >= self.hold_time:
                return self.next_state

        else:

            # No demand, is there demand now?
            # Demand is reset on each transition.
            self.demand = self.path.get_demand()
