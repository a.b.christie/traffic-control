#!/usr/bin/env python3

"""
Traffic management 'PathController' action enumerations.

Alan Christie
July 2015
"""

from enum import Enum


# -----------------------------------------------------------------------------
# PathAction
# -----------------------------------------------------------------------------
class PathAction(Enum):

    """An enumeration used in the PathController internal queue.
    Path actions control lights (go and stop) and also provide
    instructions for terminating PathController instances."""

    go = 1
    stop = 2

    terminate = 3
