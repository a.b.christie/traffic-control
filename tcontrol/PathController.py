#!/usr/bin/env python3

"""
Traffic management for a single `path`.

Alan Christie
July 2015
"""

# Import standard python stuff
import queue
import time
import threading
import datetime

# Import our stuff
import tcontrol.Control as Control
from tcontrol.PathAction import PathAction

# Module version
# and brief revision history (latest first)
__version__ = '1.3'
#   1.3     Added 'demand' mechanism & faster wait(0)
#   1.2     Added _QUEUE_SIZE
#   1.1     Now a thread with a queue of actions
#   1.0     Initial (prototype) release

# Wait pause (in seconds) between queue inspections
_WAIT_PAUSE_SECS = 0.1

# Queue size (maximum pending actions)
_QUEUE_SIZE = 4


# -----------------------------------------------------------------------------
# PathController
# -----------------------------------------------------------------------------
class PathController(threading.Thread):

    """This class controls lights for the given path identity. It is a thread
    that processes command placed on an internal queue by one of the control
    methods. The lights can be switched to 'go' or 'stop' using the
    `go()` and `stop()` control methods.

    The controller can be terminated with a call to the `terminate()`
    control method.

    Actions are delivered without delay but are processed in real-time.
    Transitioning from got to stop will take a number of seconds, defined
    ultimately by the timing constants in `Control.py`. The typical
    transition time from go to stop is 5 seconds.

    Actions are queued and so a delay may be observed in processing.
    For example if you call `go()` followed immediately by `terminate()`
    the path controller may not terminate for up to 5 seconds."""

    # -------------------------------------------------------------------------
    def __init__(self, path):

        """Initialise the class instance. This remembers the path identiy
        and sets up the object's 'action' queue."""

        # Always initialise the parent class instance...
        super().__init__()

        assert 0 < path <= Control.num_paths(), \
            "Invalid strobe ID (%s)" % path
        self.path = path

        # Create our 'action' queue.
        # STOP, START and QUIT commands are supplied to us through this queue.
        # The queue is a small, limited size. When full the queue will block.
        self.action_queue = queue.LifoQueue(_QUEUE_SIZE)

        # We operate a counter (pending count) that is incremented when
        # actions are placed in the queue and decremented after they have
        # been taken out and processed. This allows the 'wait()' method
        # to operate correctly - i.e. wait until a) the action queue
        # is empty and the event has also been processed. The value is
        # incremented for each put and decremented at the end of the run()
        # code.
        self.pending_count = 0
        self.pending_count_lock = threading.Lock()

        # Timestamp of last 'demand' input
        self.last_demand_time = None

        # Flag, cleared to exit the thread run() loop
        self.running = True

    # -------------------------------------------------------------------------
    def get_path(self):

        """Returns the path ID."""

        return self.path

    # -------------------------------------------------------------------------
    def _inc_pending(self):

        """Increment the number of unprocessed (pending) actions (safely).
        This is done by `_put()`."""

        with self.pending_count_lock:
            self.pending_count += 1

    # -------------------------------------------------------------------------
    def _dec_pending(self):

        """Decrement the number of unprocessed (pending) actions (safely).
        This is done at the end of the `run()` method's while block."""

        with self.pending_count_lock:
            assert self.pending_count
            self.pending_count -= 1

    # -------------------------------------------------------------------------
    def _pending(self):

        """Returns whether there are any pending (put and unprocessed) actions.
        This will be False when all `_put()` actions have been processed."""

        result = False
        with self.pending_count_lock:
            if self.pending_count:
                result = True

        return result

    # -------------------------------------------------------------------------
    def _put(self, action):

        """Puts an action onto the internal action queue. The number of
        pending (put but unprocessed) actions is incremented."""

        self._inc_pending()
        self.action_queue.put(action)

    # -------------------------------------------------------------------------
    def go(self):

        """Put the 'go' action on the queue. Response is immediate
        action is handled in the object's `run()` method."""

        assert self.running,\
            "Thread no longer running (%s)" % self.path

        self.last_demand_time = None
        self._put(PathAction.go)

    # -------------------------------------------------------------------------
    def demand(self):

        """Called when there's traffic demand for this path.
        The date and time of the call is recorded and used for
        switching decisions."""

        self.last_demand_time = datetime.datetime.now()

    # -------------------------------------------------------------------------
    def get_demand(self):

        """Returns the time of the last demand for this path, None
        if no demand."""

        return self.last_demand_time

    # -------------------------------------------------------------------------
    def stop(self):

        """Put the 'stop' action on the queue. Response is immediate
        action is handled in the object's `run()` method."""

        assert self.running,\
            "Thread no longer running (%s)" % self.path

        self.last_demand_time = None
        self._put(PathAction.stop)

    # -------------------------------------------------------------------------
    def terminate(self):

        """Put the 'terminate' action on the queue. Response is immediate
        action is handled in the object's `run()` method. This action
        causes the thread to (eventually) terminate."""

        assert self.running,\
            "Thread no longer running (%s)" % self.path

        self._put(PathAction.terminate)

    # -------------------------------------------------------------------------
    def wait(self, timeout):

        """Waits until the internal queue is empty. The queue is polled
        at short intervals (_WAIT_PAUSE_SECS seconds) and control is returned\
        to the caller when the queue is empty. If, at the end of the timeout
        events remain in the queue the return value is True.

        The caller can provide a +ve `timeout` value. If the specified time
        elapses before the pending events have been processed the wait method
        returns.

        If a timeout is supplied this True is returned if actions remain
        unprocessed at the end of the waiting period."""

        assert timeout >= 0,\
            "Invalid timeout (%s)" % timeout

        elapsed = 0
        too_long = False

        pending = self._pending()
        while pending and timeout > 0 and self.running and not too_long:

            # Wait (briefly) before trying again...
            time.sleep(_WAIT_PAUSE_SECS)
            pending = self._pending()

            # If there's a timeout
            # have we waited too long?
            elapsed += _WAIT_PAUSE_SECS
            if elapsed >= timeout:
                too_long = True

        # Return 'pending' state
        return pending

    # -------------------------------------------------------------------------
    def run(self):

        """The thread `run()` method. We simply sit on the back-end of the
        queue and process actions as they arrive. The `terminate` action
        causes the `run()` method to exit.

        As actions are processed the number of pending (put but unprocessed)
        actions is decremented."""

        while self.running:

            # Get from the queue, blocking until something is retrieved.
            item = self.action_queue.get()
            assert isinstance(item, PathAction),\
                "Invalid object on action_queue (%s)" % item

            # Process the action...
            if item == PathAction.go:

                # Traffic to be set to 'go' on this path
                Control.go(self.path)

            elif item == PathAction.stop:

                # Traffic to be set to 'stop' on this path
                Control.stop(self.path)

            elif item == PathAction.terminate:

                # The 'kill' event -
                # stop processing and exit the run-loop
                self.running = False

            # The action has been processed,
            # safely decrement the number of pending actions.
            self._dec_pending()
