#!/usr/bin/env python3

"""
UnbiasedCrossroads is a control wrapper around two `PathController` objects.
It implements behaviour for an _unbiased_ crossroads. An unbiased
crossroads has a set of lights for each path that only change when traffic
demand is observed. Lights do not revert to the prior path of traffic
unless demand is observed.

This class uses transition logic passed to a 'finite state machine'
driven by 'demand' from the supported paths (two in this case).

Alan Christie
July 2015
"""

# Import standard python stuff
import threading
import time

# Import our stuff
import tcontrol.PathController as PathController
import tcontrol.FiniteStateMachine as FiniteStateMachine
import tcontrol.StatePathGoToGreen as StatePathGoToGreen
import tcontrol.StatePathGoToRed as StatePathGoToRed
import tcontrol.StatePathWaitForDemand as StatePathWaitForDemand

# Module version
# and brief revision history (latest first)
__version__ = '1.0'
#   1.0     Initial (prototype) release

# Minimum and maximum times between receiving demand
# for a non-permitted path and switching to that path...
_MIN_SWITCH_TIME_S = 0.0
_MAX_SWITCH_TIME_S = 8.0


# -----------------------------------------------------------------------------
# UnbiasedCrossroads
# -----------------------------------------------------------------------------
class UnbiasedCrossroads(threading.Thread):

    """A wrapper around two path objects that implements an 'unbiased'
    crossroads. Unbiased means that the lights switch ot the path that
    presents a demand and remain in that path until demand returns on the
    alternative path."""

    # -------------------------------------------------------------------------
    def __init__(self, primary, secondary):

        """Initialise the object. Here we simply record the paths
        and create an instance of an FSM to manage the traffic-light
        sequencing."""

        super().__init__()

        # We must be given PathControllers
        assert isinstance(primary, PathController.PathController),\
            "Primary is not a PathController"
        assert isinstance(secondary, PathController.PathController),\
            "Secondary is not a PathController"
        assert primary.get_path() != secondary.get_path(),\
            "Primary and secondary paths must be different."

        # Remember the supplied paths
        self.primary = primary
        self.secondary = secondary

        # Create an instance of a finite state machine,
        # this will manage the logic managing path transitions
        self.fsm = FiniteStateMachine.FiniteStateMachine()

        # Member variable that, when cleared, terminates the thread
        self.running = True

    # -------------------------------------------------------------------------
    def _initialise_fsm(self):

        """Creates all the states for the traffic lights and passes them
        into the FSM instance. This method is normally called just as this
        object's thread is about to start."""

        assert self.fsm,\
            "There is no FSM"

        # States:
        #
        # - Primary Go To Green
        # - Wait Secondary Demand
        # - Primary Go To Red
        # - Secondary Go to Green
        # - Wait Primary Demand
        # - Secondary Go To Red (then back to first state)

        state1 = StatePathGoToGreen.\
            StatePathGoToGreen(self.primary, "wait-sec-demand")
        state2 = StatePathWaitForDemand.\
            StatePathWaitForDemand(self.secondary, 4, "pri-go-red")
        state3 = StatePathGoToRed.\
            StatePathGoToRed(self.primary, "sec-go-green")
        state4 = StatePathGoToGreen.\
            StatePathGoToGreen(self.secondary, "wait-pri-demand")
        state5 = StatePathWaitForDemand.\
            StatePathWaitForDemand(self.primary, 4, "sec-go-red")
        state6 = StatePathGoToRed.\
            StatePathGoToRed(self.secondary, "pri-go-green")

        # Load the states into the FSM

        self.fsm.add_state(state1, "pri-go-green")
        self.fsm.add_state(state2, "wait-sec-demand")
        self.fsm.add_state(state3, "pri-go-red")
        self.fsm.add_state(state4, "sec-go-green")
        self.fsm.add_state(state5, "wait-pri-demand")
        self.fsm.add_state(state6, "sec-go-red")

        # Start the path threads
        self.primary.start()
        self.secondary.start()

    # -------------------------------------------------------------------------
    def run(self):

        """The Thread's `run()` method. Here we initialise the FSM and start
        it before entering a while-loop that terminates on a call to this
         object's `stop()` method."""

        # First step -
        # Initialise the FSM and start it
        self._initialise_fsm()
        self.fsm.start()

        while self.running:
            time.sleep(0.25)

        # Leaving the thread - stop the state machine
        self.fsm.stop()

    # -------------------------------------------------------------------------
    def stop(self):

        """Clears the 'running' member variable, causing the `run()` method
        (after a short period) to end."""

        self.running = False

    # -------------------------------------------------------------------------
    def get_current_state_name(self):

        """Calls the underlying FSM to get the name of the current state."""

        assert self.fsm,\
            "No FSM"

        return self.fsm.get_current_state_name()

    # -------------------------------------------------------------------------
    def debug(self, context=0):

        """Enables debug for this and underlying objects."""

        # Enable debug in our FSM...
        self.fsm.debug(context)
