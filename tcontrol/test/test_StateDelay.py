#!/usr/bin/env python3

"""
Python unit tests for StateDelay.py.
"""

# Python modules we need...
import datetime
import time
import unittest

# The object under test...
import tcontrol.StateDelay as StateDelay


# -----------------------------------------------------------------------------
# StateDelayTests
# -----------------------------------------------------------------------------
class StateDelayTests(unittest.TestCase):

    # -------------------------------------------------------------------------
    def setUp(self):

        self.state = StateDelay.StateDelay(7, "next_state")

    # -------------------------------------------------------------------------
    def tearDown(self):

        if self.state:
            self.state = None

    # -------------------------------------------------------------------------
    def test_enter(self):

        self.state.enter()

        # Check entry time has been recorded...
        now = datetime.datetime.now()
        elapsed = now - self.state.enter_time
        max_elapsed = datetime.timedelta(0.5)
        self.assertLess(elapsed, max_elapsed)

    # -------------------------------------------------------------------------
    def test_exit(self):

        self.state.exit()

    # -------------------------------------------------------------------------
    def test_notify(self):

        # Check 'notify()' and measure the time to a yield...

        min_elapsed = datetime.timedelta(seconds=6.5)
        max_elapsed = datetime.timedelta(seconds=8.0)

        start_time = datetime.datetime.now()

        result = None
        while not result:
            result = self.state.notify()
            time.sleep(0.25)

        # Correct result?
        self.assertEqual("next_state", result)

        # Fast enough?
        finish_time = datetime.datetime.now()
        elapsed = finish_time - start_time
        self.assertGreater(elapsed, min_elapsed)
        self.assertLess(elapsed, max_elapsed)


# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------
if __name__ == '__main__':

    unittest.main()
