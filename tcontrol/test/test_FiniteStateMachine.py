#!/usr/bin/env python3

"""
Python unit tests for FiniteStateMachine.py.
"""

# Python modules we need...
import time
import unittest

# The object under test...
import tcontrol.FiniteStateMachine as FiniteStateMachine

# Globals, used by out 'dummy' state implementors.
state_one_notify_count = 0


# -----------------------------------------------------------------------------
# StateOne
# -----------------------------------------------------------------------------
class StateOne:

    def __init__(self, transit_on_notify=False):

        self.notify_count = 0
        self.exit_count = 0
        self.enter_count = 0
        self.transit_on_notify = transit_on_notify

    def enter(self):

        self.enter_count += 1

    def exit(self):

        self.exit_count += 1

    def notify(self):

        self.notify_count += 1
        if self.transit_on_notify:
            return "state2"


# -----------------------------------------------------------------------------
# StateTwo
# -----------------------------------------------------------------------------
class StateTwo:

    def __init__(self, transit_on_notify=False):

        self.notify_count = 0
        self.exit_count = 0
        self.enter_count = 0
        self.transit_on_notify = transit_on_notify

    def enter(self):

        self.enter_count += 1

    def exit(self):

        self.exit_count += 1

    def notify(self):

        self.notify_count += 1
        if self.transit_on_notify:
            return "state1"


# -----------------------------------------------------------------------------
# StateWithoutNotify
# -----------------------------------------------------------------------------
class StateWithoutNotify:

    def __init__(self):

        self.notify_count = 0

    def enter(self):

        pass

    def exit(self):

        pass


# -----------------------------------------------------------------------------
# StateTwo
# -----------------------------------------------------------------------------
class StateWithoutEnter:

    def __init__(self):

        self.notify_count = 0

    def exit(self):

        pass

    def notify(self):

        self.notify_count += 1


# -----------------------------------------------------------------------------
# StateTwo
# -----------------------------------------------------------------------------
class StateWithoutExit:

    def __init__(self):

        self.notify_count = 0

    def enter(self):

        pass

    def notify(self):

        self.notify_count += 1


# -----------------------------------------------------------------------------
# FiniteStateMachineTests
# -----------------------------------------------------------------------------
class FiniteStateMachineTests(unittest.TestCase):

    # -------------------------------------------------------------------------
    def setUp(self):

        # Create the test's FSM instance
        self.fsm = FiniteStateMachine.FiniteStateMachine()

    # -------------------------------------------------------------------------
    def tearDown(self):

        if self.fsm:
            self.fsm.stop()

    # -------------------------------------------------------------------------
    def test_state_without_notify(self):

        state = StateWithoutNotify()
        with self.assertRaises(AssertionError):
            self.fsm.add_state(state, "who-cares")

    # -------------------------------------------------------------------------
    def test_state_without_enter(self):

        state = StateWithoutEnter()
        with self.assertRaises(AssertionError):
            self.fsm.add_state(state, "who-cares")

    # -------------------------------------------------------------------------
    def test_state_without_exit(self):

        state = StateWithoutExit()
        with self.assertRaises(AssertionError):
            self.fsm.add_state(state, "who-cares")

    # -------------------------------------------------------------------------
    def test_duplicate_states(self):

        self.assertEqual(0, self.fsm.get_num_states())

        state = StateOne()
        self.fsm.add_state(state, "state1")
        self.assertEqual(1, self.fsm.get_num_states())

        with self.assertRaises(AssertionError):
            self.fsm.add_state(state, "state1")

    # -------------------------------------------------------------------------
    def test_add_state_after_start(self):

        self.fsm.add_state(StateOne(), "state1")
        self.fsm.start()

        with self.assertRaises(AssertionError):
            self.fsm.add_state(StateOne(), "state1")

    # -------------------------------------------------------------------------
    def test_start_without_states(self):

        with self.assertRaises(AssertionError):
            self.fsm.start()

    # -------------------------------------------------------------------------
    def test_notify_rate(self):

        state = StateOne()

        self.fsm.add_state(state, "state1")
        self.fsm.start()

        # Wait (say 5 seconds)
        time.sleep(5)

        self.assertGreater(state.notify_count, 3)

    # -------------------------------------------------------------------------
    def test_get_current_state_name(self):

        # Empty state should have no state name
        self.assertIsNone(self.fsm.get_current_state_name())

        state = StateOne()
        self.fsm.add_state(state, "state1")

        self.assertEqual("state1", self.fsm.get_current_state_name())

    # -------------------------------------------------------------------------
    def test_state_transition(self):

        state1 = StateOne(transit_on_notify=True)
        state2 = StateTwo(transit_on_notify=True)

        self.fsm.add_state(state1, "state1")
        self.fsm.add_state(state2, "state2")
        self.fsm.start()

        # Wait (say 5 seconds)
        time.sleep(5)

        self.assertGreater(state1.notify_count, 1)
        self.assertGreater(state1.enter_count, 0)
        self.assertGreater(state1.exit_count, 0)

        self.assertGreater(state2.notify_count, 1)
        self.assertGreater(state2.enter_count, 0)
        self.assertGreater(state2.exit_count, 0)

    # -------------------------------------------------------------------------
    def test_state_transition_with_debug(self):

        state1 = StateOne(transit_on_notify=True)
        state2 = StateTwo(transit_on_notify=True)

        self.fsm.debug()

        self.fsm.add_state(state1, "state1")
        self.fsm.add_state(state2, "state2")
        self.fsm.start()

        # Wait (say 5 seconds)
        time.sleep(5)

        self.assertGreater(state1.notify_count, 1)
        self.assertGreater(state1.enter_count, 0)
        self.assertGreater(state1.exit_count, 0)

        self.assertGreater(state2.notify_count, 1)
        self.assertGreater(state2.enter_count, 0)
        self.assertGreater(state2.exit_count, 0)
