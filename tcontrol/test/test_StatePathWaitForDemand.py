#!/usr/bin/env python3

"""
Python unit tests for StatePathWaitForDemand.py.
"""

# Python modules we need...
import datetime
import time

from unittest.mock import Mock
import unittest

# The object under test...
import tcontrol.StatePathWaitForDemand as StatePathWaitForDemand
import tcontrol.PathController as PathController


# -----------------------------------------------------------------------------
# StatePathWaitForDemandTests
# -----------------------------------------------------------------------------
class StatePathWaitForDemandTests(unittest.TestCase):

    # -------------------------------------------------------------------------
    def setUp(self):

        self.wait_seconds = 4

        self.mock_path = Mock(spec=PathController.PathController)
        self.state = StatePathWaitForDemand.\
            StatePathWaitForDemand(self.mock_path,
                                   self.wait_seconds,
                                   "next_state")

    # -------------------------------------------------------------------------
    def tearDown(self):

        if self.state:
            self.state = None

    # -------------------------------------------------------------------------
    def test_enter(self):

        self.state.enter()

    # -------------------------------------------------------------------------
    def test_exit(self):

        # Set a field that should be cleared on exit...
        self.state.demand = 1

        self.state.exit()

        # Was the field cleared?
        self.assertIsNone(self.state.demand)

    # -------------------------------------------------------------------------
    def test_notify_without_demand(self):

        # Check 'notify()' when the underlying PathController.get_demand()
        # returns None (there is no demand on the path)...

        attributes = {'get_demand.return_value': None}
        self.mock_path.configure_mock(**attributes)

        result = self.state.notify()
        self.assertIsNone(self.state.demand)
        self.assertIsNone(result)

    # -------------------------------------------------------------------------
    def test_notify_with_demand(self):

        # Check 'notify()' when the underlying PathController.get_demand()
        # returns None (there is no demand on the path)...

        now = datetime.datetime.now()
        attributes = {'get_demand.return_value': now}
        self.mock_path.configure_mock(**attributes)

        result = self.state.notify()
        self.assertEqual(now, self.state.demand)
        self.assertIsNone(result)

        # Now keep calling until the expected 'hold time' has elapsed

        pause_secs = 0.1
        time_to_next_state = 0
        while result is None and time_to_next_state < 10:
            time.sleep(pause_secs)
            result = self.state.notify()
            time_to_next_state += pause_secs

        # Did we yield the next state?
        self.assertEqual("next_state", result)
        # And did it yield at the correct time
        # (The wait time plus a small margin)
        # i.e. configured 'wait_time' +/- 0.2 seconds
        self.assertLess(time_to_next_state, self.wait_seconds + 0.2)
        self.assertGreater(time_to_next_state, self.wait_seconds - 0.2)

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------
if __name__ == '__main__':

    unittest.main()
