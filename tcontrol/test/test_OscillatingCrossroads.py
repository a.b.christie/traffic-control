#!/usr/bin/env python3

"""
Python unit tests for OscillatingCrossroads.py.
"""

# Python modules we need...
from unittest.mock import Mock
import time
import unittest

# The object under test...
import tcontrol.PathController as PathController
import tcontrol.OscillatingCrossroads as OscillatingCrossroads
import tcontrol.Control as Control


# -----------------------------------------------------------------------------
# OscillatingCrossroadsTests
# -----------------------------------------------------------------------------
class OscillatingCrossroadsTests(unittest.TestCase):

    # -------------------------------------------------------------------------
    @classmethod
    def setUpClass(cls):

        """One-time initialisation."""

        Control.init()

    # -------------------------------------------------------------------------
    def setUp(self):

        # Create mock paths...

        self.mock_path1 = Mock(spec=PathController.PathController)
        attributes = {'get_path.return_value': 1,
                      'get_demand.return_value': None,
                      'wait.return_value': True}
        self.mock_path1.configure_mock(**attributes)

        self.mock_path2 = Mock(spec=PathController.PathController)
        attributes = {'get_path.return_value': 2,
                      'get_demand.return_value': None,
                      'wait.return_value': True}
        self.mock_path2.configure_mock(**attributes)

        # Create the object under test...

        self.ocr = OscillatingCrossroads.OscillatingCrossroads(self.mock_path1,
                                                               self.mock_path2)
        self.ocr.debug()
        self.ocr.start()

    # -------------------------------------------------------------------------
    def tearDown(self):

        if self.ocr:
            self.ocr.stop()
            self.ocr.join()

    # -------------------------------------------------------------------------
    def test_basic_op(self):

        # Time to wait while checking that a state does not change...
        wait_seconds = 8
        delay_wait_seconds = 20
        wait_sleep_seconds = 0.5
        state_change_pause_seconds = 2

        #####
        # 1 #
        #####
        #
        # Here we should remain in the initial state
        # until the state has completed (which will require us
        # to mock the a 'False' return from Path1's 'wait()' method).
        # For now, just make sure the state does not change...
        #
        # The object under test is a thread so calling into it
        # comes with some risk (i.e. the state name may be
        # changing as we call into the object). We could solve this with
        # a protection mechanism in the UnbiasedCrossroads class.

        print("1->")
        print("Watching...")

        # Initially, we're stuck in the first state
        self.assertEqual("pri-go-green", self.ocr.get_current_state_name())
        # And we stay there...
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("pri-go-green", self.ocr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 2 #
        #####
        #
        # Release the initial state by 'mocking' 'False'
        # from the path's 'wait' method. This happens when (in this case)
        # the path actually gets to 'green'. The switch to the next
        # state will be quick. Here, we make sure we get to the next
        # state and remain there for the correct length of time...

        print("2->")
        print("Triggering state...")

        # Simulate 1st path going to green...
        self.unlock_path1()

        # Pause
        time.sleep(state_change_pause_seconds)
        # And lock the paths
        self.lock_paths()

        print("Watching state...")

        # Have we moved to the next state?
        self.assertEqual("pri-delay", self.ocr.get_current_state_name())

        # How long does it stay there?
        wait_time = 0
        new_state = "pri-delay"
        while wait_time < delay_wait_seconds and new_state == "pri-delay":

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            new_state = self.ocr.get_current_state_name()

            wait_time += wait_sleep_seconds

        # Did we wait for the correct length of time?
        print("Waited " + str(wait_time) + " seconds for transition")
        self.assertGreater(wait_time, 10)
        self.assertLess(wait_time, 14)

        # Correct new state?
        self.assertEqual("pri-go-red", new_state)

        #####
        # 3 #
        #####

        print("3->")
        print("Watching state...")

        # And does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("pri-go-red", self.ocr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 4 #
        #####

        print("4->")
        print("Triggering state...")

        # Simulate 1st path going to green...
        self.unlock_path1()

        # Pause
        time.sleep(state_change_pause_seconds)
        # Lock both paths
        self.lock_paths()

        print("Watching state...")

        # Have we moved to the next state?
        self.assertEqual("sec-go-green", self.ocr.get_current_state_name())

        # Does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("sec-go-green", self.ocr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 5 #
        #####

        print("5->")
        print("Triggering state...")

        # Simulate 2nd path going to green...
        self.unlock_path2()

        # Pause
        time.sleep(state_change_pause_seconds)
        # Lock paths
        self.lock_paths()

        print("Watching state...")

        # Have we moved to the next state?
        self.assertEqual("sec-delay", self.ocr.get_current_state_name())

        # Stop secondary path from changing...
        attributes = {'wait.return_value': True}
        self.mock_path2.configure_mock(**attributes)

        # How long does it stay there?
        wait_time = 0
        new_state = "sec-delay"
        while wait_time < delay_wait_seconds and new_state == "sec-delay":

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            new_state = self.ocr.get_current_state_name()

            wait_time += wait_sleep_seconds

        # Did we wait for the correct length of time?
        print("Waited " + str(wait_time) + " seconds for transition")
        self.assertGreater(wait_time, 4)
        self.assertLess(wait_time, 8)

        # Correct new state?
        self.assertEqual("sec-go-red", new_state)

        #####
        # 6 #
        #####

        print("6->")
        print("Triggering state...")

        # Simulate 1st path going to green...
        self.unlock_path2()

        # Pause
        time.sleep(state_change_pause_seconds)
        # Lock paths
        self.lock_paths()

        print("Watching state...")

        # Have we moved to the next state?
        self.assertEqual("pri-go-green", self.ocr.get_current_state_name())

        # Does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("pri-go-green", self.ocr.get_current_state_name())

            wait_time += wait_sleep_seconds

    # -------------------------------------------------------------------------
    def lock_paths(self):

        # Prevent both paths from progressing
        # (setting return-value of wait() to True)
        attributes = {'wait.return_value': True}
        self.mock_path1.configure_mock(**attributes)
        self.mock_path2.configure_mock(**attributes)

    # -------------------------------------------------------------------------
    def unlock_path1(self):

        # Unlock 1st path
        # (setting return-value of wait() to False)
        attributes = {'wait.return_value': False}
        self.mock_path1.configure_mock(**attributes)

    # -------------------------------------------------------------------------
    def unlock_path2(self):

        # Unlock 2nd path
        # (setting return-value of wait() to False)
        attributes = {'wait.return_value': False}
        self.mock_path2.configure_mock(**attributes)

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------
if __name__ == '__main__':

    unittest.main()
