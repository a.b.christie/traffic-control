#!/usr/bin/env python3

"""
Python unit tests for StatePathToGreen.py.
"""

# Python modules we need...
from unittest.mock import Mock
import unittest

# The object under test...
import tcontrol.StatePathGoToGreen as StatePathGoToGreen
import tcontrol.PathController as PathController


# -----------------------------------------------------------------------------
# StatePathToGreenTests
# -----------------------------------------------------------------------------
class StatePathToGreenTests(unittest.TestCase):

    # -------------------------------------------------------------------------
    def setUp(self):

        self.mock_path = Mock(spec=PathController.PathController)
        self.state = StatePathGoToGreen.\
            StatePathGoToGreen(self.mock_path, "next_state")

    # -------------------------------------------------------------------------
    def tearDown(self):

        if self.state:
            self.state = None

    # -------------------------------------------------------------------------
    def test_enter(self):

        self.state.enter()

        # Inspect the underlying mock object...
        self.mock_path.go.assert_called_with()

    # -------------------------------------------------------------------------
    def test_exit(self):

        self.state.exit()

    # -------------------------------------------------------------------------
    def test_notify(self):

        # Check 'notify()' when the underlying PathController.wait()
        # still indicates actions are pending...
        # (Returns True)

        attributes = {'wait.return_value': True}
        self.mock_path.configure_mock(**attributes)

        result = self.state.notify()

        # Inspect the underlying mock object...
        self.mock_path.wait.assert_called_with(0)
        self.assertIsNone(result)

        # Check 'notify()' when the underlying PathController.wait()
        # indicates actions are no longer pending...
        # (Returns False)

        attributes = {'wait.return_value': False}
        self.mock_path.configure_mock(**attributes)

        result = self.state.notify()

        # Inspect the underlying mock object...
        self.assertEqual("next_state", result)

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------
if __name__ == '__main__':

    unittest.main()
