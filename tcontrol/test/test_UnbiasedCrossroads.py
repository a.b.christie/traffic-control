#!/usr/bin/env python3

"""
Python unit tests for UnbiasedCrossroads.py.
"""

# Python modules we need...
from unittest.mock import Mock
import datetime
import time
import unittest

# The object under test...
import tcontrol.PathController as PathController
import tcontrol.UnbiasedCrossroads as UnbiasedCrossroads
import tcontrol.Control as Control


# -----------------------------------------------------------------------------
# UnbiasedCrossroadsTests
# -----------------------------------------------------------------------------
class UnbiasedCrossroadsTests(unittest.TestCase):

    # -------------------------------------------------------------------------
    @classmethod
    def setUpClass(cls):

        """One-time initialisation."""

        Control.init()

    # -------------------------------------------------------------------------
    def setUp(self):

        # Create mock paths...

        self.mock_path1 = Mock(spec=PathController.PathController)
        attributes = {'get_path.return_value': 1,
                      'get_demand.return_value': None,
                      'wait.return_value': True}
        self.mock_path1.configure_mock(**attributes)

        self.mock_path2 = Mock(spec=PathController.PathController)
        attributes = {'get_path.return_value': 2,
                      'get_demand.return_value': None,
                      'wait.return_value': True}
        self.mock_path2.configure_mock(**attributes)

        self.ucr = UnbiasedCrossroads.UnbiasedCrossroads(self.mock_path1,
                                                         self.mock_path2)
        self.ucr.debug()
        self.ucr.start()

    # -------------------------------------------------------------------------
    def tearDown(self):

        if self.ucr:
            self.ucr.stop()
            self.ucr.join()

    # -------------------------------------------------------------------------
    def test_basic_op(self):

        # Time to wait while checking that a state does not change...
        wait_seconds = 8
        wait_sleep_seconds = 1
        state_change_pause_seconds = 2

        #####
        # 1 #
        #####
        #
        # Here we should remain in the initial state
        # until the state has completed (which will require us
        # to mock the a 'False' return from Path1's 'wait()' method).
        # For now, just make sure the state does not change...
        #
        # The object under test is a thread so calling into it
        # comes with some risk (i.e. the state name may be
        # changing as we call into the object). We could solve this with
        # a protection mechanism in the UnbiasedCrossroads class.

        print("1->")
        print("Watching...")

        # Initially, we're stuck in the first state
        self.assertEqual("pri-go-green", self.ucr.get_current_state_name())
        # And we stay there...
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("pri-go-green", self.ucr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 2 #
        #####
        #
        # Release the initial state by 'mocking' 'False'
        # from the path's 'wait' method. This happens when (in this case)
        # the path actually gets to 'green'. The switch to the next
        # state will be quick. Here, we make sure we get to the next
        # state (wait for demand on path 2) and stay there...

        print("2->")
        print("Triggering state...")

        # Simulate 1st path going to green...
        self.unlock_path1()

        # Pause
        time.sleep(state_change_pause_seconds)
        # Lock both paths
        self.lock_paths()

        print("Watching state...")

        # Have we moved to the next state?
        self.assertEqual("wait-sec-demand", self.ucr.get_current_state_name())

        # Does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("wait-sec-demand", self.ucr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 3 #
        #####

        print("3->")
        print("Triggering state...")

        # Simulate demand on path 2...
        attributes = {'get_demand.return_value': datetime.datetime.now()}
        self.mock_path2.configure_mock(**attributes)

        # How long does it take to change state?
        # Should be just over 4 seconds but be generous...
        min_elapsed = datetime.timedelta(seconds=3.9)
        max_elapsed = datetime.timedelta(seconds=5.5)
        start_time = datetime.datetime.now()
        while self.ucr.get_current_state_name() == "wait-sec-demand":
            time.sleep(0.1)
        elapsed = datetime.datetime.now() - start_time

        print("Elapsed=" + str(elapsed))

        self.assertGreater(elapsed, min_elapsed)
        self.assertLess(elapsed, max_elapsed)

        print("Watching state...")

        # Is it the right state?
        self.assertEqual("pri-go-red", self.ucr.get_current_state_name())

        # And does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("pri-go-red", self.ucr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 4 #
        #####

        print("4->")
        print("Triggering state...")

        # Simulate 1st path going to green...
        self.unlock_path1()

        # Pause
        time.sleep(state_change_pause_seconds)
        # Lock paths
        self.lock_paths()

        print("Watching state...")

        # Have we moved to the next state?
        self.assertEqual("sec-go-green", self.ucr.get_current_state_name())

        # Does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("sec-go-green", self.ucr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 5 #
        #####

        print("5->")
        print("Triggering state...")

        # Simulate 1st path going to green...
        self.unlock_path2()

        # Pause
        time.sleep(state_change_pause_seconds)
        # Lock paths
        self.lock_paths()

        print("Watching state...")

        # Have we moved to the next state?
        self.assertEqual("wait-pri-demand", self.ucr.get_current_state_name())

        # Does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("wait-pri-demand", self.ucr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 6 #
        #####

        print("6->")
        print("Triggering state...")

        # Simulate demand on path 1...
        attributes = {'get_demand.return_value': datetime.datetime.now()}
        self.mock_path1.configure_mock(**attributes)

        # How long does it take to change state?
        start_time = datetime.datetime.now()
        while self.ucr.get_current_state_name() == "wait-pri-demand":
            time.sleep(0.1)
        elapsed = datetime.datetime.now() - start_time

        print("Elapsed=" + str(elapsed))

        self.assertGreater(elapsed, min_elapsed)
        self.assertLess(elapsed, max_elapsed)

        print("Watching state...")

        # Is it the right state?
        self.assertEqual("sec-go-red", self.ucr.get_current_state_name())

        # And does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("sec-go-red", self.ucr.get_current_state_name())

            wait_time += wait_sleep_seconds

        #####
        # 7 #
        #####

        print("7->")
        print("Triggering state...")

        # Simulate 2ns path going to red...
        self.unlock_path2()

        # Pause
        time.sleep(state_change_pause_seconds)
        # Lock paths
        self.lock_paths()

        print("Watching state...")

        # Have we moved to the next state?
        self.assertEqual("pri-go-green", self.ucr.get_current_state_name())

        # Does it stay there?
        wait_time = 0
        while wait_time < wait_seconds:

            # Sleep
            time.sleep(wait_sleep_seconds)
            # Check again...
            self.assertEqual("pri-go-green", self.ucr.get_current_state_name())

            wait_time += wait_sleep_seconds

    # -------------------------------------------------------------------------
    def lock_paths(self):

        # Prevent both paths from progressing
        # (setting return-value of wait() to True)
        attributes = {'wait.return_value': True}
        self.mock_path1.configure_mock(**attributes)
        self.mock_path2.configure_mock(**attributes)

    # -------------------------------------------------------------------------
    def unlock_path1(self):

        # Unlock 1st path
        # (setting return-value of wait() to False)
        attributes = {'wait.return_value': False}
        self.mock_path1.configure_mock(**attributes)

    # -------------------------------------------------------------------------
    def unlock_path2(self):

        # Unlock 2nd path
        # (setting return-value of wait() to False)
        attributes = {'wait.return_value': False}
        self.mock_path2.configure_mock(**attributes)

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------
if __name__ == '__main__':

    unittest.main()
