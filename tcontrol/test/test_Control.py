#!/usr/bin/env python3

"""
Python unit tests for Control.py.
"""

# Python modules we need..
import unittest

# The object under test...
import tcontrol.Control as Control


# -----------------------------------------------------------------------------
# ControlTests
# -----------------------------------------------------------------------------
class ControlTests(unittest.TestCase):

    # -------------------------------------------------------------------------
    @classmethod
    def setUpClass(cls):

        """One-time initialisation."""

        Control.init()

    # -------------------------------------------------------------------------
    def test_red(self):

        Control.red(1)
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertFalse(a)
        self.assertFalse(b)
        self.assertEqual(1, path)

    # -------------------------------------------------------------------------
    def test_amber(self):

        Control.amber(1)
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertTrue(a)
        self.assertFalse(b)
        self.assertEqual(1, path)

    # -------------------------------------------------------------------------
    def test_green(self):

        Control.green(1)
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertTrue(a)
        self.assertTrue(b)
        self.assertEqual(1, path)

    # -------------------------------------------------------------------------
    def test_red_amber(self):

        Control.red_amber(1)
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertFalse(a)
        self.assertTrue(b)
        self.assertEqual(1, path)

    # -------------------------------------------------------------------------
    def test_go(self):

        Control.go(1)
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertTrue(a)
        self.assertTrue(b)
        self.assertEqual(1, path)

    # -------------------------------------------------------------------------
    def test_stop(self):

        Control.stop(1)
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertFalse(a)
        self.assertFalse(b)
        self.assertEqual(1, path)

    # -------------------------------------------------------------------------
    def test_reset(self):

        Control.reset()
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertFalse(a)
        self.assertFalse(b)
        self.assertEqual(Control.num_paths(), path)

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------
if __name__ == '__main__':

    unittest.main()
