#!/usr/bin/env python3

"""
Python unit tests for PathController.py.
"""

# Python modules we need...
import datetime
import unittest

# The object under test...
import tcontrol.PathController as PathController
import tcontrol.Control as Control


# -----------------------------------------------------------------------------
# PathControllerTests
# -----------------------------------------------------------------------------
class PathControllerTests(unittest.TestCase):

    # -------------------------------------------------------------------------
    @classmethod
    def setUpClass(cls):

        """One-time initialisation."""

        Control.init()

    # -------------------------------------------------------------------------
    def test_get_path(self):

        controller_one = PathController.PathController(1)
        self.assertEqual(1, controller_one.get_path())

    # -------------------------------------------------------------------------
    def test_go(self):

        controller_one = PathController.PathController(1)
        controller_one.start()

        # Trigger the change...
        controller_one.go()

        # Wait...
        controller_one.wait(10)
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertTrue(a)
        self.assertTrue(b)
        self.assertEqual(1, path)

        # Terminate...
        controller_one.terminate()
        controller_one.join()

    # -------------------------------------------------------------------------
    def test_stop(self):

        controller_one = PathController.PathController(2)
        controller_one.start()

        # Trigger the change...
        controller_one.stop()

        # Wait...
        controller_one.wait(10)
        # Validate...
        a, b, path = Control.get_last_set_data()
        self.assertFalse(a)
        self.assertFalse(b)
        self.assertEqual(2, path)

        # Terminate...
        controller_one.terminate()
        controller_one.join()

    # -------------------------------------------------------------------------
    def test_wait_short_timeout(self):

        controller_one = PathController.PathController(2)
        controller_one.start()

        # Trigger the change...
        controller_one.stop()

        # Wait (making sure it's too short)...
        pending = controller_one.wait(0.1)
        self.assertTrue(pending)

        # Terminate...
        controller_one.terminate()
        controller_one.join()

    # -------------------------------------------------------------------------
    def test_wait_long_timeout(self):

        controller_one = PathController.PathController(2)
        controller_one.start()

        # Trigger the change...
        controller_one.stop()

        # Wait (making sure it's too short)...
        pending = controller_one.wait(15)
        self.assertFalse(pending)

        # Terminate...
        controller_one.terminate()
        controller_one.join()

    # -------------------------------------------------------------------------
    def test_demand(self):

        controller_one = PathController.PathController(1)
        controller_one.start()

        # Read initial demand (None)
        demand = controller_one.get_demand()
        self.assertIsNone(demand)

        # Issue demand
        test_time = datetime.datetime.now()
        controller_one.demand()
        demand = controller_one.get_demand()
        diff = demand - test_time
        max_diff = datetime.timedelta(milliseconds=50)
        self.assertLess(diff, max_diff)

        # Terminate...
        controller_one.terminate()
        controller_one.join()

    # -------------------------------------------------------------------------
    def test_zero_path_controller(self):

        with self.assertRaises(AssertionError):
            PathController.PathController(0)

    # -------------------------------------------------------------------------
    def test_invalid_path_controller(self):

        with self.assertRaises(AssertionError):
            PathController.PathController(Control.num_paths() + 1)

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------
if __name__ == '__main__':

    unittest.main()
